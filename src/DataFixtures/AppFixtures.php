<?php

namespace App\DataFixtures;

use App\Entity\Admin;
use App\Entity\Comment;
use App\Entity\Conference;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

class AppFixtures extends Fixture
{

    private PasswordHasherFactoryInterface $passwordHashesFactory;

    public function __construct(PasswordHasherFactoryInterface $encoderFactory)
    {
        $this->passwordHashesFactory = $encoderFactory;
    }

    public function load(ObjectManager $manager): void
    {
        $amsterdam = new Conference();
        $amsterdam->setCity('Barcelone');
        $amsterdam->setYear('2021');
        $amsterdam->setIsInternational(false);
        $manager->persist($amsterdam);

        $paris = new Conference();
        $paris->setCity('Paris');
        $paris->setYear('2022');
        $paris->setIsInternational(true);
        $manager->persist($paris);

        $comment1 = new Comment();
        $comment1->setConference($amsterdam);
        $comment1->setAuthor('Fabien');
        $comment1->setPhotoFilename('18eb256cf5b2.png');
        $comment1->setEmail('fabien@example.com');
        $comment1->setText('Je reviendrai');
        $comment1->setState('published');
        $manager->persist($comment1);

        $comment2 = new Comment();
        $comment2->setConference($paris);
        $comment2->setAuthor('Lucas');
        $comment2->setPhotoFilename('18eb256cf5b2.png');
        $comment2->setEmail('lucas@example.com');
        $comment2->setText('Lourd !');
        $comment2->setState('submitted');
        $manager->persist($comment2);

        $comment3 = new Comment();
        $comment3->setConference($paris);
        $comment3->setAuthor('Quentin');
        $comment3->setPhotoFilename('18eb256cf5b2.png');
        $comment3->setEmail('quentin@example.com');
        $comment3->setText('Bête de conf !');
        $comment3->setState('published');
        $manager->persist($comment3);

        $comment4 = new Comment();
        $comment4->setConference($paris);
        $comment4->setAuthor('Jean-michel');
        $comment4->setPhotoFilename('18eb256cf5b2.png');
        $comment4->setEmail('jm@example.com');
        $comment4->setText('Génial !');
        $comment4->setState('submitted');
        $manager->persist($comment4);

        $admin = new Admin();
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setUsername('admin');
        $admin->setPassword($this->passwordHashesFactory->getPasswordHasher(Admin::class)->hash('admin', null));
        $manager->persist($admin);

        $manager->flush();
    }
}
